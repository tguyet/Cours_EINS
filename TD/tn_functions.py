import networkx as nx
import numpy as np

# Implementation of the magic function that checks whether the temporal constraints
# are satisfiable and, if it is the case, it minimizes the temporal network. It creates the 
# equivalent temporal constraint network with the narrowest temporal intervals.
# More specifically, it adds the temporal contraints that are missing in the initial network.

def minimization(G):
    """Minimization a graph temporal network"""

    # create a simple temporal network
    G_stp = nx.DiGraph()
    G_stp.add_nodes_from(G.nodes)
    for e in list(G.edges):
        G_stp.add_edge( e[0], e[1], weight=G[e[0]][e[1]]['I'][1])
        G_stp.add_edge( e[1], e[0], weight=-G[e[0]][e[1]]['I'][0])
    
    for n in range(len(G.nodes)):
        try:
            #negative cycle: there is no solution
            nx.find_negative_cycle(G_stp,n)
            return None
        except:
            pass
    #find shortest paths
    adj=nx.floyd_warshall_numpy(G_stp)

    #recreate the minimized graph temporal network
    G_out = nx.DiGraph()
    G_out.add_nodes_from(G.nodes)
    for n in range(len(G.nodes)):
        for m in range(n+1,len(G.nodes)):
            G_out.add_edge(n,m)
            G_out[n][m]['I'] = (-adj[m][n], adj[n][m])
            
    return G_out