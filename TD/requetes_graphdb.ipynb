{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exemple de requêtes temporelles sur une base de données medico-administrative"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Mise en place de la base de données\n",
    "\n",
    "Cette section décrit la mise en place de la base de données au sein de GraphDB."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "On fournit une base de données synthétiques en format RDF. Cette base de données est issue d'un processus de génération en deux étapes :\n",
    "1. une base de données dans une structure similaire à celle du SNDS a été générée. Cette base reproduit certaines statistiques globales connues dans les données, mais n'utilise pas de modèle complexe (e.g. des modèles de deep learning pour apprendre et générer de nouvelles données). Les données sont ainsi totalement anonymes et peuvent être partagées sans limitation liée à la sensibilité des données de santé. Ce générateur est librement disponible et utilisable (https://gitlab.inria.fr/tguyet/medtrajectory_datagen/), il a été développé en collaboration avec Aymeric Florac (HdH).\n",
    "2. dans un second temps, nous avons transformé les données de la base de données relationnelle sous un formation du web sémantique. Nous avons pour cela conçu une ontologie réutilisant des ontologies du domaines telles que la CCAM pour les actes médicaux, l'ATC pour la clasification des médicaments.\n",
    "\n",
    "C'est cette dernière base que nous vous proposons d'explorer dans la suite. Mais avant de pouvoir l'explorer, nous allons la mettre en place dans une base de données sémantique. Pour ce TP, nous avons choisi l'outil [GraphDB](https://graphdb.ontotext.com/). Cet outil intègre également les fonctionnalités permettant d'interroger avec le langage SPARQL. \n",
    "Plus d'informations sont disponibles sur cet outil dans sa [documentation](https://graphdb.ontotext.com/documentation/10.2/).\n",
    "\n",
    "Commençons d'abord par récupérer les données (trop grosses pour être mises sur le git):\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!wget http://people.irisa.fr/Thomas.Guyet/documents/patients.trig\n",
    "!wget http://people.irisa.fr/Thomas.Guyet/documents/care_providers.ttl"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "#### Quelques mots sur le modèle de GraphDB\n",
    "\n",
    "Le modèle des données de GraphDB est orienté vers l'utilisation de *graphes nommés*. Cette notion de graphe nommée fait partie intégrante des normes du web sémantique, mais sont moins présentes dans les outils usuels (Jena/Virtuoso).\n",
    "\n",
    "Un *graphe nommé* définit un contexte dans lequel sont définis des triplets RDF classiques. L'exemple ci-dessous illustre une base comportant 2 graphes (chaque graphe représente un patient). \n",
    "On retrouve alors que `:pat rdf:instanceOf snds:Patient.` dans les deux graphes, mais ces faits sont en fait distincts puisqu'ils sont dans deux graphes (contextes) différents.\n",
    "\n",
    "<font size=\"1\">\n",
    "\n",
    "```\n",
    "GRAPH :pat123{\n",
    "        :pat rdf:instanceOf snds:Patient;\n",
    "        snds:has_sex 1 ;\n",
    "        snds:has_birthdate \"2019-02-22\"^^xsd:date.\n",
    "        \n",
    "        :pat snds:has_event :EVT1.\n",
    "        :EVT1 rdf:instanceOf snds:DrugDelivery;\n",
    "        snds:has_cip \"3400949656516\";\n",
    "}\n",
    "\n",
    "GRAPH :pat782{\n",
    "        :pat rdf:instanceOf snds:Patient;\n",
    "        snds:has_sex 0 ;\n",
    "        snds:has_birthdate \"2001-12-02\"^^xsd:date.\n",
    "        \n",
    "        :pat snds:has_event :EVT1.\n",
    "        :EVT1 rdf:instanceOf snds:DrugDelivery;\n",
    "        snds:has_cip \"3400358455787\";\n",
    "}\n",
    "```\n",
    "</font>\n",
    "\n",
    "Dans le cas de notre base de données, nous aurons un graphe par patient. Chaque graphe décrit le parcours de ce patient. \n",
    "Nous verrons plus loin comment faire des requêtes SPARQL adaptées à de telles données.\n",
    "\n",
    "Toutes les bases de graphes disposent d'un *graphe par défaut* dans lequel des connaissances non-contextualisées (hors-graphes) peuvent être ajoutées. Il est donc tout à fait possible d'utiliser GraphDB comme un serveur de données du web sémantique sans graphes."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Lancement du serveur GraphDB\n",
    "\n",
    "Dans un premier temps, il faut lancer le serveur GraphDB, installé dans la machine virtuelle. Pour cela, il faut lancer la commande suivante depuis un terminal\n",
    "\n",
    "``` bash\n",
    "$ ~/Tools/graphdb-10.2.2/bin/graphdb &\n",
    "```\n",
    "\n",
    "Une fois cette commande exécutée, le ``serveur'' fonctionne. Il doit rester en fonctionnement pendant toute la durée de l'utilisation des données.\n",
    "\n",
    "Il est alors possible d'accéder de manière interactive aux fonctionnalités du serveur GraphDB au travers d'une interface web accessible par ce lien : http://localhost:7200/. Dans l'ordre des boutons situés à gauche de l'interface, vous pouvez :\n",
    "* Importer des données dans la base de données\n",
    "* Explorer de manière interactive les données et connaissances\n",
    "* Requêter vos données à l'aide du language SPARQL\n",
    "* ou encore faire des tâches d'administration de la base de données."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Importation de données et connaissances\n",
    "\n",
    "Depuis l'onglet `Importer`, l'ajout de nouvelles données se fait d'abord en chargeant le fichier RDF (ici, nous utiliserons des fichiers statiques mis à disposition), puis en utilisant les boutons `Importer` qui apparaisse à droite des fichiers chargés. Lorsque l'opération se passe bien, les données sont ensuite disponibles dans les graphes des données.\n",
    "\n",
    "Pour notre cas d'application, les données sont mises à disposition dans le répertoire ̀`data` qui contient trois types de données (l'ordre de présentation est ici logique, mais n'est en fait pas du tout indispensable à respecter pour le chargement des données):\n",
    "1. tout d'abord, on dispose des connaissances du domaine : \n",
    "    * `CIM-10_enriched.owl`, `ATC.ttl`, `time.rdf`: la description des maladies et des médicaments,\n",
    "    * `snds.rdf`: il s'agit d'ontologie décrivant le schéma de données de nos trajectoires de soins\n",
    "2. ensuite, le fichier `care_providers.ttl` contient de données indépendantes des patients (en particulier, il s'agit des personnels de santé et des structures de santé qui sont mentionnées dans les descriptions des évènements médicaux). Ces données se trouveront dans le *graphe par défaut*. \n",
    "3. finalement, `patients.trig` contient les descriptions des parcours de soin selon le schéma défini dans `snds.ttl`.\n",
    "\n",
    "\n",
    "Vous **chargerez donc l'ensemble de ces fichiers** pour constituer votre base de données.\n",
    "\n",
    "#### Exploration des données\n",
    "\n",
    "Une fois le chargement effectué, vous pourrez aller *explorer* le graphe en parcourant certains graphes à l'aide de l'aperçu (il y a des aperçus sous forme de table et également sous forme de graphes).\n",
    "\n",
    "Pour commencer l'exploration, nous vous invitons à commencer par explorer le schéma des données tel que défini dans `snds.ttl`. Cette ontologie définit le vocabulaire pour les concepts et les relations qui seront utilisées pour décrire les données. Il est donc important d'en avoir une idée générale pour savoir comment intérroger les données. \n",
    "\n",
    "L'interface de GraphDB permet de visualiser les ontologies sous forme graphique au moyen de l'onglet `class hierarchy`, mais cette visualisation est un peu limitée. Pour l'exploration de l'ontologie, nous vous invitons plutôt à l'explorer avec l'outil Protegé.\n",
    "\n",
    "\n",
    "Dans un second temps, on vous invite à explorer les données avec la vue tabulaire de GraphDB, il est possible de suivre les objets, prédicats, sujets et contextes par leurs relations dans les données.\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Requêter les données\n",
    "\n",
    "Cette section décrit comment requêter les données à l'aide de SPARQL, dans l'interface et via Python."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Premières requêtes\n",
    "\n",
    "Maintenant que les données sont accessibles, et que vous disposez d'une vue d'ensemble des concepts et relations qui sont dans cette base de données, il est possible de les requêter en SPARQL.\n",
    "\n",
    "Dans un premier temps, nous vous invitons à formuler vos requêtes au moyen de l'interface graphique (on verra ci-dessous) qu'il est possible de les faire également via Python.\n",
    "\n",
    "* Forme générale d'une requête sur une base de données de graphes: La requête ci-dessous permet de lister les 100 premiers couple de graphe / relation. On voit qu'il est possible ainsi d'utiliser le mot clé `GRAPH` pour invoquer l'identifiant d'un graphe dans une requête.\n",
    "\n",
    "<font size=\"1\">\n",
    "\n",
    "```\n",
    "select distinct ?g ?r where { \n",
    "    GRAPH ?g {?s ?r ?o .}\n",
    "} limit 100 \n",
    "```\n",
    "</font>\n",
    "\n",
    "* Requêtes sur les patients: quels sont les patients qui sont nés avant 1930 ? Notez que la requête fait appel à des préfixes, il est donc nécessaire de les définir dans la requête.\n",
    "\n",
    "<font size=\"1\">\n",
    "\n",
    "```\n",
    "PREFIX snds: <http://localhost/ontologies/snds#>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n",
    "\n",
    "SELECT ?p ?date WHERE { \n",
    "\tGRAPH ?p {\n",
    "        ?pat snds:has_birthdate ?date.\n",
    "    }\n",
    "    FILTER (?date < \"1930-01-01\"^^xsd:date)\n",
    "} limit 100 \n",
    "``` \n",
    "</font>\n",
    "\n",
    " \n",
    "* Exemple de requête de comptage : nombre de patients ayant reçu au moins un médicament de la classe `C09AA04`. On peut noter qu'il n'y a pas besoin de `GRAPH` dans cette requête, il est donc possible d'accéder directement aux triplets de manière classique.\n",
    "\n",
    "<font size=\"1\">\n",
    "\n",
    "```sparql\n",
    "SELECT (count(distinct ?p) as ?count) WHERE { \n",
    "    ?p snds:has_event ?e. \n",
    "    ?e snds:deliver atc:C09AA04.\n",
    "}\n",
    "```\n",
    "</font>\n",
    "\n",
    "* Exemple d'utilisation des hiérarchies de la classification ATC (adaptation de la requête précédente): nombre de patients ayant reçu au moins un médicament de la classe `C09AA`. On utilise ici la relation `subClassOf*` pour déterminer la nature du médicament.\n",
    "\n",
    "<font size=\"1\">\n",
    "\n",
    "```sparql\n",
    "SELECT (count(distinct ?p) as ?count) WHERE { \n",
    "    ?p snds:has_event ?e. \n",
    "    ?e snds:deliver ?c.\n",
    "    ?c rdfs:subClassOf* atc:C09AA.\n",
    "}\n",
    "```\n",
    "</font>\n",
    "\n",
    "\n",
    "* Requête sur le nombre de prescription de `C09AA04` par médecins.\n",
    "\n",
    "<font size=\"1\">\n",
    "\n",
    "```sparql\n",
    "SELECT ?pres (count(distinct ?e) as ?count)  WHERE {\n",
    "    ?p snds:has_event ?e. \n",
    "    ?e snds:deliver atc:C09AA04.\n",
    "    ?e snds:has_prescriber ?pres.\n",
    "} group by ?pres order by ?count\n",
    "```\n",
    "</font>\n",
    "\n",
    "\n",
    "Autres idées de requêtes :\n",
    "\n",
    "* quelles sont les personnes qui ont été à l'hopital ?\n",
    "* quelles sont les personnes qui ont été à l'hopital pour cause d'AVC (code CIM I63 par exemple) ?\n",
    "* quels sont les codes de spécialités des médecins pour lesquels il y a eu des visites\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Execution d'une requête en python et récupération des résultats\n",
    "\n",
    "Dans cette section, l'objectif est de présenter l'utilisation de Python pour faire des requêtes SPARQL. Pour cela, nous utilisons principalement deux librairies : la librairie `sparql-dataframe` et la librairie `pandas`. \n",
    "\n",
    "La librairie `sparql-dataframe` fait l'interface avec le serveur SPARQL  (endpoint) pour récupérer les résultats d'une requête sous la forme d'un tableau `pandas` qui est un format assez standard pour la manipulation de données tabulaires."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# On commence par installer les paquets nécessaires si ils ne sont pas déjà disponibles.\n",
    "%pip install SPARQLWrapper\n",
    "%pip install sparql-dataframe\n",
    "%pip install pandas"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On peut maintenant illustrer l'utilisation d'un requête en Python."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sparql_dataframe\n",
    "import pandas as pd\n",
    "\n",
    "#definition du endpoint à utiliser: remplacer le nom de la base par celui \n",
    "# qui est indiqué en haut à droite dans l'interface de GraphDB.\n",
    "endpoint = \"http://portable-tg:7200/repositories/Test_DB\"\n",
    "\n",
    "# Definition de la requête (copier-coller de l'interface de GraphDB)\n",
    "q = \"\"\"PREFIX snds: <http://localhost/ontologies/snds#>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n",
    "\n",
    "SELECT ?p ?date WHERE { \n",
    "\tGRAPH ?p {\n",
    "        ?pat snds:has_birthdate ?date.\n",
    "    }\n",
    "    FILTER (?date < \"1930-01-01\"^^xsd:date)\n",
    "} limit 100 \n",
    "\"\"\"\n",
    "\n",
    "#Commande magique pour récupérer les résultats en tableau pandas\n",
    "df = sparql_dataframe.get(endpoint, q)\n",
    "\n",
    "df"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Requêter les parcours de soins avec des motifs temporels\n",
    "\n",
    "Dans cette section, on combine raisonnement ontologique et temporel pour requêter les données par des patterns temporels expressifs."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Description du pattern d'intérêt\n",
    "\n",
    "\n",
    "Dans cette base de données, on s'intéresse à des patients qui ont été hospitalisés pour des AVC. Pour détecter de tels patients, on s'intéresse aux parcours suivants :\n",
    "\n",
    "```\n",
    "Patients qui ont été hospitalisé avec un diagnostic codé I60, I61, I62, I63, ou I64 dans la CIM-10, puis qui ont eu une visite chez un Angiologue  (code spécialité 3) entre 2 et 4 mois après l'hospitalisation, et qui ont eu une délivrance d'anticoagulant (classe ATC 'B01AC') dans le mois suivant (délivrée avant 35 jours après la sortie).\n",
    "```\n",
    "\n",
    "**Vous pourrez commencer par représenter cette situation sous la forme d'une chronicle**"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Requête SPARQL\n",
    "\n",
    "La requête ci-dessous illustre la possibilité d'exprimer la situation d'intérêt sous la forme d'une requête SPARQL. Pour simplifier, on se limite au cas des codes CIM I61. "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```\n",
    "PREFIX atc: <http://purl.bioontology.org/ontology/ATC/>\n",
    "PREFIX snds: <http://localhost/ontologies/snds#>\n",
    "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n",
    "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n",
    "PREFIX cim: <http://chu-rouen.fr/cismef/CIM-10#>\n",
    "PREFIX time: <http://www.w3.org/2006/time#>\n",
    "PREFIX db: <http://localhost/ontologies/db/>\n",
    "PREFIX ofn: <http://www.ontotext.com/sparql/functions/>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n",
    "\n",
    "select ?p ?d1 ?d2 ?d3 (ofn:asDays(?d3-?d2) as ?diff )where { \n",
    "    GRAPH ?p {\n",
    "        ?e1 rdf:instanceOf snds:DrugDelivery.\n",
    "        ?e1 snds:deliver ?c.\n",
    "        ?e1 snds:has_time ?t1.\n",
    "        ?t1 time:has_beginning ?d1.\n",
    "        \n",
    "        ?e2 rdf:instanceOf snds:ShortStay.\n",
    "        ?e2 snds:diag_principal ?diag.\n",
    "        ?e2 snds:has_time ?t2.\n",
    "        ?t2 time:has_end ?d2.\n",
    "     \t \n",
    "        ?e3 rdf:instanceOf snds:Visit.\n",
    "        ?e3 snds:has_time ?t3.\n",
    "        ?t3 time:has_beginning ?d3.\n",
    "        ?e3 snds:has_performer ?medecin.\n",
    "    }\n",
    "    ?diag rdfs:subClassOf* cim:I61.\n",
    "    ?c rdfs:subClassOf* atc:B01AC.\n",
    "    \n",
    "    FILTER(ofn:asDays(?d3 -?d2)>60 ).\n",
    "    FILTER(ofn:asDays(?d3 -?d2)<120 ).\n",
    "    FILTER(ofn:asDays(?d1 -?d2)>0 ).\n",
    "    FILTER(ofn:asDays(?d1 -?d2)<35 ).\n",
    "}\n",
    "```"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Même si cette requête semble plutôt simple, son temps d'exécution peut devenir important avec de grosses bases de données."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Utilisation de requêtes par évènements\n",
    "\n",
    "Dans cette partie, nous souhaitons spécifier le parcours de soin à reconnaitre au travers de formalismes issues de l'analyse de données temporelles. Plus particulièrement, on propose d'utiliser :\n",
    "\n",
    "* la Metric Temporal Logic \n",
    "* les chroniques\n",
    "\n",
    "Pour cela, nous nous appuierons sur la librairie [PyChronicles](https://tguyet.gitlabpages.inria.fr/pychronicles) qui fonctionne à partir de données représentées dans des tableaux Pandas. On commence donc par \"découper\" la requête ci-dessus en autant de requêtes que d'évènements d'intérêt.\n",
    "L'intérêt de l'approche est ici de conserver toute l'expressivité du web sémantique pour définir les évènements, et de bénéficier des raisonnements temporels efficaces et riches."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dans la cellule suivante, on définit les trois requêtes utiles pour définir les évènements du parcours de soins."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prefix = \"\"\"PREFIX atc: <http://purl.bioontology.org/ontology/ATC/>\n",
    "PREFIX snds: <http://localhost/ontologies/snds#>\n",
    "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n",
    "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n",
    "PREFIX cim: <http://chu-rouen.fr/cismef/CIM-10#>\n",
    "PREFIX time: <http://www.w3.org/2006/time#>\n",
    "PREFIX db: <http://localhost/ontologies/db/>\n",
    "PREFIX ofn: <http://www.ontotext.com/sparql/functions/>\n",
    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\"\"\"\n",
    "\n",
    "queries={}\n",
    "\n",
    "#definition de l'évènement de délivrance de médicaments\n",
    "queries['dd']=prefix + \"\"\"\n",
    "select distinct ?p ?d where { \n",
    "    GRAPH ?p {\n",
    "        ?e1 rdf:instanceOf snds:DrugDelivery.\n",
    "        ?e1 snds:deliver ?c.\n",
    "        ?e1 snds:has_time ?t1.\n",
    "        ?t1 time:has_beginning ?d.\n",
    "    }\n",
    "    ?c rdfs:subClassOf* atc:B01AC.\n",
    "}\"\"\"\n",
    "\n",
    "#definition de l'évènement d'hospitalisation\n",
    "queries['hosp']=prefix + \"\"\"\n",
    "select distinct ?p ?d where { \n",
    "    GRAPH ?p {\n",
    "        ?e2 rdf:instanceOf snds:ShortStay.\n",
    "        ?e2 snds:diag_principal ?diag.\n",
    "        ?e2 snds:has_time ?t2.\n",
    "        ?t2 time:has_end ?d.\n",
    "    }\n",
    "    ?diag rdfs:subClassOf* cim:I61.\n",
    "}\"\"\"\n",
    "\n",
    "#definition de l'évènement de visite chez un médecin (sans spécialisation particulière)\n",
    "queries['visit']=prefix + \"\"\"\n",
    "select distinct ?p ?d where { \n",
    "    GRAPH ?p {\n",
    "        ?e3 rdf:instanceOf snds:Visit.\n",
    "        ?e3 snds:has_time ?t3.\n",
    "        ?t3 time:has_beginning ?d.\n",
    "        ?e3 snds:has_performer ?medecin.\n",
    "    }\n",
    "}\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# exécution des requêtes pour en faire un tableau Pandas\n",
    "df = pd.DataFrame()\n",
    "\n",
    "for evt_name,query in queries.items():\n",
    "    ldf = sparql_dataframe.get(endpoint, query)\n",
    "    ldf['evt']=evt_name\n",
    "    df = pd.concat( (df,ldf))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On applique maintenant quelques transformations des données pour récupérer le \"bon\" format et faciliter l'utilisation par la suite"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import urllib\n",
    "pd.set_option('display.max_colwidth', None) #<- affichage des url complètes pour accéder aux données\n",
    "\n",
    "# transformation des dates \n",
    "df['d']=pd.to_datetime(df['d'])\n",
    "df=df.set_index(\"d\")\n",
    "\"\"\"df['val']=True\n",
    "df=df.pivot(index=['p','d'],columns='evt', values='val').fillna(False)\"\"\"\n",
    "#creation d'une url cliquable d'un côté et d'un code numérique d'identification de l'autre\n",
    "df['url']=df['p'].apply( lambda x : \"http://localhost:7200/resource?uri=\"+urllib.parse.quote_plus(x,safe=\":\")+\"&role=context\" )\n",
    "df['p']=pd.to_numeric(df['p'].str.lstrip(\" http://localhost/ontologies/\").str.replace('B','0').str.replace('A','0'))\n",
    "df.head()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On peut maintenant utiliser des outils pour analyser ce tableau qui décrit les évènements des trajectoires de soins qui correspondent à ceux qui sont mentionnés dans le parcours de soin d'intérêt."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%pip install -r ./pychronicles/requirements.txt\n",
    "import sys\n",
    "sys.path.append(\"./pychronicles/\")\n",
    "import pychronicles\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c=pychronicles.Chronicle()\n",
    "c.add_event(0, 'evt==\"dd\"')\n",
    "c.add_event(1, 'evt==\"hosp\"')\n",
    "c.add_constraint(0, 1, (np.timedelta64(0,'D'), np.timedelta64(35,'D')))\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ret=df.groupby('p').apply(lambda d: d.tpattern.match(c))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ret.sum()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Si on reprend la requêtes précédente, on peut retrouner les patients qui correspondent à cette requête en la retraduisant de la manière suivante:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c=pychronicles.Chronicle()\n",
    "c.add_event(0, 'evt==\"dd\"')\n",
    "c.add_event(1, 'evt==\"hosp\"')\n",
    "c.add_event(2, 'evt==\"visit\"')\n",
    "c.add_constraint(0, 1, (np.timedelta64(0,'D'), np.timedelta64(35,'D')))\n",
    "c.add_constraint(1, 2, (np.timedelta64(60,'D'), np.timedelta64(120,'D')))\n",
    "ret=df.groupby('p').apply(lambda d: d.tpattern.match(c))\n",
    "\n",
    "patients_id=ret[ret==True].reset_index()['p']\n",
    "patients_id"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#get the urls of patients (and clic on them to access them in the GraphDB interface):\n",
    "df[['p','url']].drop_duplicates().merge(patients_id, on='p')['url']"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Il est également possible de requêter les données à l'aide de formules MTL.\n",
    "Dans le cas de la chronique précédente, il est possible de définir une formule MTL équivalente."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_mtl = df.reset_index()\n",
    "df_mtl['d']=(df_mtl['d']-np.datetime64(\"2022-01-01\")).dt.days\n",
    "df_mtl=df_mtl.set_index('d')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "query='F(evt==\"dd\" & F[0,35](evt==\"hosp\" & F[60,120](evt==\"visit\") ) )'\n",
    "ret=df_mtl.groupby('p').apply(lambda d: d.tpattern.match_mtl(query))\n",
    "ret"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sur ce résultat ... il y a un soucis puisqu'on s'attendrait à avoir le même qu'avant ! Je n'ai pas eu le temps de trouver l'explication !!"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### À vous de jouer"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dans cette section, on vous décrit une situation clinique qui correspond à l'étude épidémiologique GENEPI qui s'intéressait au lien entre la survenue de crises épileptiques chez les patients épileptiques stables et le changement de prescription de médicaments (en particulier, le passage à des médicaments génériques).\n",
    "\n",
    "Pour plus d'information sur cette étude, vous pouvez consulter l'article suivant :\n",
    "```bibtex\n",
    "Polard E, Nowak E, Happe A, Biraben A, Oger E; GENEPI Study Group. Brand name to generic substitution of antiepileptic drugs does not lead to seizure-related hospitalization: a population-based case-crossover study. Pharmacoepidemiol Drug Saf. 2015 Nov;24(11):1161-9. doi: 10.1002/pds.3879. Epub 2015 Sep 23. PMID: 26395769.\n",
    "```\n",
    "\n",
    "Dans la suite, nous avons adapté les détails de l'étude pour que cela correspondent au besoin pédagogique, et nous rappellons que les données sur lesquels vous travaillez sont fictives. Aucune conclusion médicale solide ne peut être faite à partir de celles-ci."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Patients épileptiques stables\n",
    "\n",
    "On commence par identifier les patients d'intérêt de l'étude : les patients épileptiques stables. Ce sont les patients dont on estime qu'ils ont un traitement déjà bien en place. Ils sont identifiés comme sont ceux qui ont eu, dans l'année civile, au moins 10 délivrances de médicaments anti-épileptiques. Il faut également que ces patients aient eu une visite chez un neurologue dans l'année (médecin dont la spécilité est codée 3 dans le SNDS). \n",
    "Nous éliminons aussi les patients qui ont une ALD identifiée (toute ALD).\n",
    "\n",
    "Les médicaments anti-éplileptiques d'intérêt sont `N03AF01`, `N03AX09`, `N03AX14`, `N03AX11`, `N03AG01` et `N03AF02`.\n",
    "\n",
    "*Précision* La base de données a été générée pour une année civile (2022). Certains évènements peuvent néanmoins dépasser 2022.\n",
    "\n",
    "L'objectif est d'identifier les patients ces patients selon une ou plusieurs de ces approches :\n",
    "\n",
    "* concevoir une requêtes SPARQL permettant de répondre à l'ensemble de ces critères\n",
    "* concevoir une chronique sémantiques pour décrire la situation présentée : nous commencerez par définir les évènements d'intérêt comme des requêtes SPARQL simple (sans filtre temporel), puis définirez une chronique pour representer les aspects temporels\n",
    "* implémenter en python votre chroniques et exécutez là sur la base de données"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Aide (pour une solution avec chroniques)\n",
    "queries={}\n",
    "\n",
    "#1: define the semantic queries\n",
    "queries['...']=\"\"\" SELECT { ... } \"\"\"\n",
    "queries['...']=\"\"\" SELECT { ... } \"\"\"\n",
    "\n",
    "# exécution des requêtes pour en faire un tableau Pandas\n",
    "df = pd.DataFrame()\n",
    "for evt_name,query in queries.items():\n",
    "    ldf = sparql_dataframe.get(endpoint, query)\n",
    "    ldf['evt']=evt_name\n",
    "    df = pd.concat( (df,ldf))\n",
    "# transformation des dates \n",
    "df['d']=pd.to_datetime(df['d'])\n",
    "df=df.set_index(\"d\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#definition d'une chronique\n",
    "c=pychronicles.Chronicle()\n",
    "\n",
    "#..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#execution de la requete\n",
    "ret=df.groupby('p').apply(lambda d: d.tpattern.match(c))\n",
    "#print out the positive answers\n",
    "ret[ret==True]"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Identification des situations relatives à l'étude GenEpi\n",
    "\n",
    "Dans cette partie, on s'intéresse à la situation relative à l'identification du lien entre changement de prescription et survenue de crise. On définit un changement de prescription par le fait de passer d'un anti-épileptique d'une classe ATC à une autre (parmis les précédentes).\n",
    "L'étude GENEPI a proposé de mener une étude sous la forme d'un *case-crossover*. \n",
    "Le principe est d'avoir un patient qui est son propre contrôle. Pour cela, on sépare pour un patient qui a eu une crise :\n",
    "* la période de *case* qui est la période de 3 mois avant la crise (avec une période d'induction de 3 jours) \n",
    "* la période de *contrôle* qui est la période de 3 mois précédent la période de case\n",
    "\n",
    "On s'intéresse alors à quatre situations :\n",
    "* patient qui a un changement de prescription dans la période de *case* et de *contrôle* \n",
    "* patient qui a un changement de prescription dans la période de *case* mais pas dans celle de *contrôle*\n",
    "* patient qui n'a eu de changement de prescription ni dans la période de *case* ni de *contrôle* \n",
    "* patient qui a un changement de prescription dans la période de *contrôle* mais pas dans celle de *case*\n",
    "\n",
    "La survenue d'une crise est identifiée par un passage à l'hopital par un code CIM du type `G40.x` ou `G41.x`. Le changement est défini comme un changement de classe de médicament anti-épileptique.\n",
    "\n",
    "Dans cette partie, on souhaite donc dénombrer chacune de ces situations, pour cela :\n",
    "\n",
    "* avec SPARQL\n",
    "    * définir et exécuter une/des requêtes SPARQL qui permettrai(en)t de répondre à la question (pas de solution unique)\n",
    "* avec des chroniques\n",
    "    1. définir **des** chroniques sémantiques qui les définissent (a priori, il faudra plusieurs chroniques)\n",
    "        * définir des évènements à comparer (au travers de requêtes SPARQL d'identification d'évènements complexes)\n",
    "        * définir les contraintes temporelles entre ces évènements\n",
    "    2. exécuter les requêtes\n",
    "    3. afficher le tableau des résultats et conclure sur l'effet du changement de prescription sur la survenue de la crise dans cette base synthétique"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#TODO"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "eins_venv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
