# Cours EINS

[EINS](https://eins.griis.ca/) est une école d'été interdisciplinaire en numérique de la santé, organisée par le GRIIS (Canada, Sherbrooke).

Dans le cadre de cette école d'été, j'ai préparé une activité autour de la ``Modélisation et raisonnement sur les parcours de soins''.

Retrouvez ici les différentes ressources pour ce cours.
